import 'dart:math';

void printComplexNumber(ComplexNumber z) {
  String result = "";
  if (z.x != 0) result += z.x.toString();
  if (z.y != 0) {
    if (z.y > 0 && z.x != 0) result += "+";
    result += z.y.toString() + "i";
  }
  if (result == "") result += "0";
  print(result);
}

class ComplexNumber {
  num x;
  num y;
  ComplexNumber({this.x = 0, this.y = 0});
  ComplexNumber operator +(ComplexNumber z) {
    return ComplexNumber(x: this.x + z.x, y: this.y + z.y);
  }

  ComplexNumber operator -(ComplexNumber z) {
    return ComplexNumber(x: this.x - z.x, y: this.y - z.y);
  }

  ComplexNumber operator *(ComplexNumber z) {
    return ComplexNumber(
        x: this.x * z.x - this.y * z.y, y: this.x * z.y + this.y * z.x);
  }

  ComplexNumber inv() {
    final sup = this.module * this.module;
    return ComplexNumber(x: this.x / sup, y: -this.y / sup);
  }

  ComplexNumber operator /(ComplexNumber z) {
    return this * (z.inv());
  }

  get module {
    return sqrt(this.x * this.x + this.y * this.y);
  }

  get argument {
    return atan(this.y / this.x);
  }
}
